import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  varLocales: string;
  constructor() {
    this.varLocales = "Este es el login se cambia así ";

    setTimeout(() => {
      this.varLocales = "Cambio de Mensaje";

    }, 2000);
  }

  ngOnInit(): void {
  }

}
