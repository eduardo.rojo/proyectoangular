/*
****Configuracion de Servidor de Node.js*****
const http = require('http');

const server = http.createServer((req, res) => {
    res.status = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello MA. Ya estoy en el seridor!!!');
});
server.listen(3000, () => {
    console.log('Server on port 3000');
});
*/

//Servidor de Express 
const express = require('express');
const app = express();

//Ptición a la ruta inicial
app.get('/', (req, res) => {
    res.send('Envia Mensaje');
});

app.get('/abaut', (req, res,) => {
    res.send('Metodo Abaut');
});

app.get('/login', (req, res,) => {
    res.send('Metodo Login');
});

//Levantar servidor en el puerto 3000
app.listen(3000, () => {
    console.log('server on port 3000');
});